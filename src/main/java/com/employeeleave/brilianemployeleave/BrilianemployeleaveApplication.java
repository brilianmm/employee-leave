package com.employeeleave.brilianemployeleave;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@SpringBootApplication
@EnableJpaAuditing
public class BrilianemployeleaveApplication {

	public static void main(String[] args) {
		SpringApplication.run(BrilianemployeleaveApplication.class, args);
	}

}
