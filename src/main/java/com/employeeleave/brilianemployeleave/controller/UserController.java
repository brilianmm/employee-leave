package com.employeeleave.brilianemployeleave.controller;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.employeeleave.brilianemployeleave.dto.UsersDTO;
import com.employeeleave.brilianemployeleave.models.PositionLeave;
import com.employeeleave.brilianemployeleave.models.Users;
import com.employeeleave.brilianemployeleave.repository.PositionLeaveRepository;
import com.employeeleave.brilianemployeleave.repository.UsersRepository;

@RestController
@RequestMapping("/api/users")
public class UserController {

	@Autowired
	UsersRepository usersRepository;
	
	@Autowired
	PositionLeaveRepository pLRepository;
	
	ModelMapper modelMapper = new ModelMapper();
	
	//create API
	@PostMapping("/create")
	public HashMap<String, Object> create(@Valid @RequestBody Users body){
		//create map for response
		HashMap<String, Object> result = new HashMap<String, Object>();
		ArrayList<PositionLeave> list = (ArrayList<PositionLeave>)pLRepository.findAll();
		
		// Create a Object ENtity and map from dto with modal mapper
		Users userEntity = modelMapper.map(body, Users.class);

		short leaveBalance = getLeaveBalance(list, userEntity.getPositions().getPositionId());
		userEntity.setLeaveBalance(leaveBalance);
		userEntity.setCreatedBy(userEntity.getName());
		userEntity.setCreatedDate(java.sql.Date.valueOf(LocalDate.now()));
		userEntity.setUpdatedBy(userEntity.getName());
		userEntity.setUpdateDate(java.sql.Date.valueOf(LocalDate.now()));
		// saving data to repository
		usersRepository.save(userEntity);

		// take the ID that was insert before
		body.setUserId(userEntity.getUserId());

		result.put("Status", "OK");
		result.put("Message", "successfully created");
		result.put("Data", body);

		return result;
	}
	
	
	@GetMapping("/read")
	public HashMap<String, Object> getAll(){
		HashMap<String, Object> resp = new HashMap<String, Object>();
		List<Users> list = usersRepository.findAll();
		List<UsersDTO> listDTO = new ArrayList<UsersDTO>();
		
		for(Users users : list) {
			UsersDTO usersDTO = modelMapper.map(users, UsersDTO.class);			
			listDTO.add(usersDTO);
		}
		resp.put("status", 200);
		resp.put("message", "Success! Data All Reviewers.");
		resp.put("data", listDTO);
		return resp;
	}
	
	@GetMapping("/read/{id}")
	public HashMap<String, Object> getById(@PathVariable(value = "id") Long userId) {
		HashMap<String, Object> resp = new HashMap<String, Object>();
		Users users = usersRepository.findById(userId).get();

		UsersDTO usersDTO = modelMapper.map(users, UsersDTO.class);	
		
		resp.put("status", 200);
		resp.put("message", "Success!");
		resp.put("data", usersDTO);
		return resp;
	}
	
	@PutMapping("/update/{id}")
	public HashMap<String, Object> update(@PathVariable(value = "id") Long userId, @Valid @RequestBody Users body) {
		HashMap<String, Object> result = new HashMap<String, Object>();
		Users users = usersRepository.findById(userId).get();
		
		body.setUserId(users.getUserId());
		
		// Create a Object ENtity and map from dto with modal mapper
		users = modelMapper.map(body, Users.class);
		
		users.setCreatedBy(users.getCreatedBy());
		users.setCreatedDate(users.getCreatedDate());
		users.setUpdatedBy(users.getName());
		users.setUpdateDate(java.sql.Date.valueOf(LocalDate.now()));
		
		// saving data to repository
		usersRepository.save(users);

		// take the ID that was insert before
		body.setUserId(users.getUserId());

		result.put("Status", "OK");
		result.put("Message", "successfully updated");
		result.put("Data", body);

		return result;
	}
	
	@DeleteMapping("/delete/{id}")
	public HashMap<String, Object> delete(@PathVariable(value = "id") Long userId){
		HashMap<String, Object> resp = new HashMap<String, Object>();
		Users users = usersRepository.findById(userId).get();	
		
		resp.put("status", 200);
		resp.put("message", "Success! Delete data from database.");
		
		usersRepository.delete(users);
		
		return resp;
	}
	
	public static short getLeaveBalance(ArrayList<PositionLeave> list, long id) {
		short balance = 0;
		for (PositionLeave p : list) {
			if (p.getPositions().getPositionId() == id) {
				balance = p.getMaxLeaveDay();
			}
		}
		
		return balance;
	}
}
