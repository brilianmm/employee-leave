package com.employeeleave.brilianemployeleave.controller;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.employeeleave.brilianemployeleave.dto.PositionsDTO;
import com.employeeleave.brilianemployeleave.models.Positions;
import com.employeeleave.brilianemployeleave.repository.PositionsRepository;

@RestController
@RequestMapping("/api/positions")
public class PositionsController {
	@Autowired
	PositionsRepository repository;
	ModelMapper modelMapper = new ModelMapper();
	
	//create API
	@PostMapping("/create")
	public HashMap<String, Object> create(@Valid @RequestBody Positions body){
		//create map for response
		HashMap<String, Object> result = new HashMap<String, Object>();

		// Create a Object ENtity and map from dto with modal mapper
		Positions entity = modelMapper.map(body, Positions.class);
		entity.setCreatedBy("admin");
		entity.setCreatedDate(java.sql.Date.valueOf(LocalDate.now()));
		entity.setUpdatedBy("admin");
		entity.setUpdatedDate(java.sql.Date.valueOf(LocalDate.now()));
		
		// saving data to repository
		repository.save(entity);

		// take the ID that was insert before
		body.setPositionId(entity.getPositionId());

		result.put("Status", "OK");
		result.put("Message", "successfully created");
		result.put("Data", body);

		return result;
	}
	
	
	@GetMapping("/read")
	public HashMap<String, Object> getAll(){
		HashMap<String, Object> resp = new HashMap<String, Object>();
		List<Positions> list = repository.findAll();
		List<PositionsDTO> listDTO = new ArrayList<PositionsDTO>();
		
		for(Positions entity : list) {
			PositionsDTO dto = modelMapper.map(entity, PositionsDTO.class);			
			listDTO.add(dto);
		}
		resp.put("status", 200);
		resp.put("message", "Success! Data All Reviewers.");
		resp.put("data", listDTO);
		return resp;
	}
	
	@GetMapping("/read/{id}")
	public HashMap<String, Object> getById(@PathVariable(value = "id") Long id) {
		HashMap<String, Object> resp = new HashMap<String, Object>();
		Positions entity = repository.findById(id).get();

		PositionsDTO dto = modelMapper.map(entity, PositionsDTO.class);	
		
		resp.put("status", 200);
		resp.put("message", "Success!");
		resp.put("data", dto);
		return resp;
	}
	
	@PutMapping("/update/{id}")
	public HashMap<String, Object> update(@PathVariable(value = "id") Long id, @Valid @RequestBody Positions body) {
		HashMap<String, Object> result = new HashMap<String, Object>();
		Positions entity = repository.findById(id).get();
		
		body.setPositionId(entity.getPositionId());
		
		// Create a Object ENtity and map from dto with modal mapper
		entity = modelMapper.map(body, Positions.class);
		entity.setCreatedBy(entity.getCreatedBy());
		entity.setCreatedDate(entity.getCreatedDate());
		entity.setUpdatedBy("admin");
		entity.setUpdatedDate(java.sql.Date.valueOf(LocalDate.now()));

		// saving data to repository
		repository.save(entity);

		// take the ID that was insert before
		body.setPositionId(entity.getPositionId());

		result.put("Status", "OK");
		result.put("Message", "successfully created");
		result.put("Data", body);

		return result;
	}
	
	@DeleteMapping("/delete/{id}")
	public HashMap<String, Object> delete(@PathVariable(value = "id") Long id){
		HashMap<String, Object> resp = new HashMap<String, Object>();
		Positions entity = repository.findById(id).get();
		
		resp.put("status", 200);
		resp.put("message", "Success! Delete data from database.");
		
		repository.delete(entity);
		
		return resp;
	}
}
