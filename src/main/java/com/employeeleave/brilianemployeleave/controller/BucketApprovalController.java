package com.employeeleave.brilianemployeleave.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.employeeleave.brilianemployeleave.dto.BucketApprovalDTO;
import com.employeeleave.brilianemployeleave.models.BucketApproval;
import com.employeeleave.brilianemployeleave.models.UserLeaveRequest;
import com.employeeleave.brilianemployeleave.models.Users;
import com.employeeleave.brilianemployeleave.repository.BucketApprovalRepository;
import com.employeeleave.brilianemployeleave.repository.PositionLeaveRepository;
import com.employeeleave.brilianemployeleave.repository.UserLeaveRequestRepository;
import com.employeeleave.brilianemployeleave.repository.UsersRepository;

@RestController
@RequestMapping("/api/resolveLeaveRequest")
public class BucketApprovalController {
	@Autowired
	UserLeaveRequestRepository repository;

	@Autowired
	PositionLeaveRepository pLRepository;

	@Autowired
	UsersRepository uRepository;

	@Autowired
	BucketApprovalRepository bRepository;

	ModelMapper modelMapper = new ModelMapper();

	@PostMapping("/create/{id}")
	public HashMap<String, Object> create(@PathVariable(value = "id") Long userId, @Valid @RequestBody BucketApprovalDTO body) {
		// create map for response
		HashMap<String, Object> result = new HashMap<String, Object>();
		ArrayList<UserLeaveRequest> uLlist = (ArrayList<UserLeaveRequest>)repository.findAll();
		ArrayList<BucketApproval> bList = (ArrayList<BucketApproval>) bRepository.findAll();
		ArrayList<Users> uList = (ArrayList<Users>) uRepository.findAll();
		
		if (idValidation(bList, body) == false) {
			result.put("Status", "ERROR");
			result.put("Message", "permohonan dengan id " + body.getBucketApprovalId() + " tidak ditemukan");
		}
		else if (solverValidation(body, bList, uLlist, uList, userId) == false) {
			result.put("Status", "ERROR");
			result.put("Message", "Anda tidak berhak melakukan approval cuti");
		}
		else if (dateValidation(body, bList, uLlist) == false) {
			result.put("Status", "ERROR");
			result.put("Message", "Kesalahan Data, Tanggal keputusan tidak bisa lebih awal dari tanggal pengajuan");
		}
		if (solverValidation(body, bList, uLlist, uList, userId) == true && idValidation(bList, body) == true && dateValidation(body, bList, uLlist) == true){
			BucketApproval bEntity = bRepository.findById(body.getBucketApprovalId()).get();
			UserLeaveRequest entity = repository.findById(bEntity.getUserLeaveRequest().getUserLeaveRequestId()).get();
			
			bEntity.setStatus(body.getStatus());
			entity.setStatus(body.getStatus());
			bEntity.setUserLeaveRequest(entity);
			bEntity.setResolverReason(body.getResolverReason());
			bEntity.setResolvedBy(body.getResolvedBy());
			bEntity.setUpdatedBy(getName(userId, uList));
			bEntity.setUpdatedDate(body.getResolvedDate());

			bRepository.save(bEntity);
			
			if (bEntity.getStatus().equalsIgnoreCase("approved")) {
				for (Users user : uList) {
					if(user.getUserId() == entity.getUsers().getUserId()) {
						int balance = getLeaveBalance(entity, uList);
						int day = getLeaveDay(entity, uLlist);
						int total = balance - day;
						short leaveRemaining = (short) total;
						user.setLeaveBalance(leaveRemaining);
						entity.setLeaveDayRemaining(leaveRemaining);
						
						uRepository.save(user);
					}
				}
			}
			
			result.put("Status", "OK");
			result.put("Message", "permohonan dengan id " + bEntity.getBucketApprovalId() + " telah diputuskan");
		}
		
		return result;
	}
	
	public static short getLeaveDay(UserLeaveRequest body, ArrayList<UserLeaveRequest> uList) {
		short leaveDay = 0;
		int diff = 0;
		for (UserLeaveRequest ul : uList) {
			if (body.getUserLeaveRequestId() == ul.getUserLeaveRequestId()) {
				if(ul.getLeaveDateFrom().getMonth()< ul.getLeaveDateTo().getMonth()) {
					if (ul.getLeaveDateFrom().getMonth() == 1 || ul.getLeaveDateFrom().getMonth() == 3 || ul.getLeaveDateFrom().getMonth() == 5 || 
							ul.getLeaveDateFrom().getMonth() == 7 || ul.getLeaveDateFrom().getMonth() == 8 || ul.getLeaveDateFrom().getMonth() == 10 ||
							ul.getLeaveDateFrom().getMonth() == 11 ) {
						diff = (31 - ul.getLeaveDateFrom().getDay()) + ul.getLeaveDateTo().getDay();
						leaveDay = (short) diff;
					}
					else if (ul.getLeaveDateFrom().getMonth() == 2) {
						diff = (28 - ul.getLeaveDateFrom().getDay()) + ul.getLeaveDateTo().getDay();
						leaveDay = (short) diff;
					}
					else {
						diff = (30 - ul.getLeaveDateFrom().getDay()) + ul.getLeaveDateTo().getDay();
						leaveDay = (short) diff;
					}
				}
				else {
					diff = ul.getLeaveDateTo().getDay() - ul.getLeaveDateFrom().getDay();
					leaveDay = (short) diff;
				}
			}
		}
		
		
		return leaveDay;
	}
	
	public static short getLeaveBalance(UserLeaveRequest body, ArrayList<Users> uList) {
		long id = 0;
		short leaveRemaining = 0;
		for (Users u : uList) {
			if (u.getUserId() == body.getUsers().getUserId()) {
				id = u.getPositions().getPositionId();
				leaveRemaining = u.getLeaveBalance();
			}
		}
		
		return leaveRemaining;
		
	}
	
	public static boolean idValidation(ArrayList<BucketApproval> blist, BucketApprovalDTO body) {
		boolean isAvailable = false;
		for (BucketApproval b : blist) {
			if (body.getBucketApprovalId() == b.getBucketApprovalId()) {
				isAvailable = true;
			}
		}
		return isAvailable;
	}
	
	public static boolean solverValidation(BucketApprovalDTO body, ArrayList<BucketApproval> bList, ArrayList<UserLeaveRequest> uLlist, ArrayList<Users> uList, long userId) {
		boolean isTrue = false;
		long ulId = getUserLeaveId(body, bList);
		long uId = getUserId(uLlist, userId, ulId);
		long posId = getPosId(uList, userId);
		
		for (Users u : uList) {
			if (u.getUserId() == uId) {
				if (u.getPositions().getPositionId() == 1) {
					if (posId == 2) {
						isTrue = true;
					}
					
				}
				if (u.getPositions().getPositionId() == 2) {
					if (posId == 2 && userId != uId) {
						isTrue = true;
					}
				}
				if (u.getPositions().getPositionId() == 3) {
					if (posId == 3) {
						isTrue = true;
					}
				}
			}
		}
		return isTrue;
		
	}
	
	public static long getUserLeaveId(BucketApprovalDTO body, ArrayList<BucketApproval> bList) {
		long ulId = 0;
		for (BucketApproval b : bList) {
			if (body.getBucketApprovalId() == b.getBucketApprovalId()) {
				ulId = b.getUserLeaveRequest().getUserLeaveRequestId();
			}
		}
		
		return ulId;
	}
	
	public static long getUserId(ArrayList<UserLeaveRequest> uLlist, long userId, long ulId) {
		long uId = 0;
		for (UserLeaveRequest ul : uLlist) {
			if (ulId == ul.getUserLeaveRequestId()) {
				uId = ul.getUsers().getUserId();
			}
		}
		
		return uId;
	}
	
	public static long getPosId(ArrayList<Users> uList, long userId) {
		long posId = 0;
		for (Users u : uList) {
			if (userId == u.getUserId()) {
				posId = u.getPositions().getPositionId();
			}
		}
		
		return posId;
	}
	
	public static String getName(long userId, ArrayList<Users> uList) {
		String name = "";
		for (Users u : uList) {
			if (u.getUserId() == userId) {
				name = u.getName();
			}
		}
		return name;
		
	}
	
	public static Date getLeaveDateFrom(BucketApprovalDTO body, ArrayList<BucketApproval> bList, ArrayList<UserLeaveRequest> uLlist) {
		Date leaveDateFrom = null;
		long ulId = getUserLeaveId(body, bList);
		for (UserLeaveRequest ul : uLlist) {
			if (ul.getUserLeaveRequestId() == ulId) {
				leaveDateFrom = ul.getLeaveDateFrom();
			}
		}
		
		return leaveDateFrom;
	}
	
	public static boolean dateValidation(BucketApprovalDTO body, ArrayList<BucketApproval> bList, ArrayList<UserLeaveRequest> uLlist) {
		boolean isTrue = false;
		Date leaveDateFrom = getLeaveDateFrom(body, bList, uLlist);
		Date resolvedDate = body.getResolvedDate();
		
		if (resolvedDate.getMonth() >= leaveDateFrom.getMonth()) {
			if (resolvedDate.getDay() >= leaveDateFrom.getDay()) {
				isTrue = true;
			}
		}
		
		return isTrue;
	}
}
