package com.employeeleave.brilianemployeleave.controller;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.Period;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.employeeleave.brilianemployeleave.dto.UserLeaveRequestDTO;
import com.employeeleave.brilianemployeleave.models.BucketApproval;
import com.employeeleave.brilianemployeleave.models.PositionLeave;
import com.employeeleave.brilianemployeleave.models.UserLeaveRequest;
import com.employeeleave.brilianemployeleave.models.Users;
import com.employeeleave.brilianemployeleave.repository.BucketApprovalRepository;
import com.employeeleave.brilianemployeleave.repository.PositionLeaveRepository;
import com.employeeleave.brilianemployeleave.repository.UserLeaveRequestRepository;
import com.employeeleave.brilianemployeleave.repository.UsersRepository;

@RestController
@RequestMapping("/api/leaveRequest")
public class UserLeaveRequestController {
	
	@Autowired
	UserLeaveRequestRepository repository;
	
	@Autowired
	PositionLeaveRepository pLRepository;
	
	@Autowired
	UsersRepository uRepository;
	
	@Autowired
	BucketApprovalRepository bRepository;
	
	ModelMapper modelMapper = new ModelMapper();
	
	@PostMapping("/create")
	public HashMap<String, Object> create(@Valid @RequestBody UserLeaveRequest body){
		//create map for response
		HashMap<String, Object> result = new HashMap<String, Object>();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		
		ArrayList<Users> uList = (ArrayList<Users>) uRepository.findAll();
		
		if (!leaveBalanceValidation(body, uList)) {
			result.put("Status", "ERROR");
			result.put("Message", "Jatah Cuti Sudah Habis");
		}
		else if (!dateNowValidation(body)) {
			result.put("Status", "ERROR");
			result.put("Message", "Backdate, Tanggal yang anda ajukan telah lampau");
		}
		else if (!rangeLeaveValidation(body, uList)) {
			String dateFrom = sdf.format(body.getLeaveDateFrom());
			String dateTo = sdf.format(body.getLeaveDateTo());
			result.put("Status", "ERROR");
			result.put("Message", "Jatah Cuti anda tidak cukup untuk digunakan dari tanggal "+ dateFrom + " sampai " + dateTo +", sisa cuti anda tersisa " + getLeaveDay(body, uList) + " hari");
		}
		else if (dateValidation(body) == false) {
			result.put("Status", "ERROR");
			result.put("Message", "Tanggal yang diajukan tidak valid");
		}
		if (leaveBalanceValidation(body, uList) && rangeLeaveValidation(body, uList) && dateValidation(body) == true && dateNowValidation(body)) {
			// Create a Object ENtity and map from dto with modal mapper
			UserLeaveRequest entity = modelMapper.map(body, UserLeaveRequest.class);
			short leaveDayRemaining = 0;
			leaveDayRemaining = getLeaveDayByPosId(entity, uList);
			entity.setLeaveDayRemaining(leaveDayRemaining);
			entity.setStatus("waiting");
			entity.setLeaveRequestDate(java.sql.Date.valueOf(LocalDate.now()));
			entity.setCreatedBy(getName(body, uList));
			entity.setCreatedDate(java.sql.Date.valueOf(LocalDate.now()));
			entity.setUpdatedBy(getName(body, uList));
			entity.setUpdatedDate(java.sql.Date.valueOf(LocalDate.now()));
			// saving data to repository
			repository.save(entity);
			
			BucketApproval bEntity = new BucketApproval();
			bEntity.setStatus(entity.getStatus());
			bEntity.setUserLeaveRequest(entity);
			bEntity.setCreatedBy(getName(body, uList));
			bEntity.setCreatedDate(java.sql.Date.valueOf(LocalDate.now()));
			bEntity.setUpdatedBy(getName(body, uList));
			bEntity.setUpdatedDate(java.sql.Date.valueOf(LocalDate.now()));
			
			bRepository.save(bEntity);

			result.put("Status", "OK");
			result.put("Message", "permohonan cuti Anda sedang diproses");
		}
		
		return result;
	}
	
	@GetMapping("/get/{id}/{dataPerPage}/{pageNumber}")
	public HashMap<String, Object> get(@PathVariable(value = "id") Long userId, @PathVariable(value = "dataPerPage") int data, @PathVariable(value = "pageNumber") int number){
		//create map for response
		HashMap<String, Object> result = new HashMap<String, Object>();

		ArrayList<UserLeaveRequest> list = repository.getPaging(userId, data, (number-1)*data);
		ArrayList<UserLeaveRequestDTO> listDTO = new ArrayList<UserLeaveRequestDTO>();
		
		UserLeaveRequestDTO dto = new UserLeaveRequestDTO();
		for (UserLeaveRequest u : list) {
			dto = modelMapper.map(u, UserLeaveRequestDTO.class);
			listDTO.add(dto);
		}
		
		result.put("Data", listDTO);
		result.put("total", listDTO.size());
		
		return result;
	}
	
	@PostMapping("/resetLeaveBalance")
	public HashMap<String, Object> reset(){
		//create map for response
		HashMap<String, Object> result = new HashMap<String, Object>();

		ArrayList<Users> uList = (ArrayList<Users>) uRepository.findAll();
		ArrayList<PositionLeave> pList = (ArrayList<PositionLeave>) pLRepository.findAll();
		
		for (Users u : uList) {
			long id = u.getUserId();
			short balance = getLeaveBalance(pList, u.getPositions().getPositionId());
			Users user = uRepository.findById(id).get();
			
			user.setLeaveBalance(balance);
			uRepository.save(user);
		}
		

		result.put("Status", "OK");
		result.put("Message", "Reset Jatah Cuti Telah Berhasil Diproses");
		
		return result;
	}
	
	public static boolean leaveBalanceValidation(UserLeaveRequest body, ArrayList<Users> uList) {
		boolean isAvailable = true;
		short dayReamaining = getLeaveDayByPosId(body, uList);
			if (dayReamaining == 0) {
				isAvailable = false;
			}
		
		return isAvailable;
	}
	
	public static short getLeaveBalance(ArrayList<PositionLeave> list, long id) {
		short balance = 0;
		for (PositionLeave p : list) {
			if (p.getPositions().getPositionId() == id) {
				balance = p.getMaxLeaveDay();
			}
		}
		
		return balance;
	}
	
	public static short getLeaveDayByPosId(UserLeaveRequest body, ArrayList<Users> uList) {
		long id = 0;
		short leaveRemaining = 0;
		for (Users u : uList) {
			if (u.getUserId() == body.getUsers().getUserId()) {
				id = u.getPositions().getPositionId();
				leaveRemaining = u.getLeaveBalance();
			}
		}
		
		return leaveRemaining;
		
	}
	
	public static short getLeaveDay(UserLeaveRequest body, ArrayList<Users> uList) {
		long id = 0;
		short leaveRemaining = 0;
		for (Users u : uList) {
			if (u.getUserId() == body.getUsers().getUserId()) {
				id = u.getPositions().getPositionId();
				leaveRemaining = u.getLeaveBalance();
			}
		}
		
		return leaveRemaining;
		
	}
	
	public static boolean dateValidation(UserLeaveRequest body) {
		boolean isTrue = true;
		if(body.getLeaveDateFrom().getMonth() == body.getLeaveDateTo().getMonth()) {
			if (body.getLeaveDateFrom().getDate() > body.getLeaveDateTo().getDate()) {
				isTrue = false;
			}
		}
		return isTrue;
	}
	
	public static boolean dateNowValidation(UserLeaveRequest body) {
		boolean isTrue = true;
		Date now = new Date();
		if (body.getLeaveDateFrom().getMonth() < now.getMonth()) {
			isTrue = false;
		} else {
			if (body.getLeaveDateFrom().getDate() < now.getDate()) {
				isTrue = false;
			}
		}
		return isTrue;
	}
	
	public static boolean rangeLeaveValidation(UserLeaveRequest body, ArrayList<Users> uList) {
		boolean isAvailable = true;
		
		Date now = new Date();
		
		Period diff = Period.between(body.getLeaveDateFrom().toInstant().atZone(ZoneId.systemDefault()).toLocalDate(), body.getLeaveDateTo().toInstant().atZone(ZoneId.systemDefault()).toLocalDate());
		short dayReamaining = getLeaveDayByPosId(body, uList);
		short leaveDay = (short) diff.getDays();
			
		if (leaveDay>dayReamaining) {
			isAvailable = false;
		}
		
		return isAvailable;
	}
	
	public static String getName(UserLeaveRequest body, ArrayList<Users> uList) {
		long id = 0;
		String name = "";
		for (Users u : uList) {
			if (u.getUserId() == body.getUsers().getUserId()) {
				id = u.getPositions().getPositionId();
				name = u.getName();
			}
		}
		return name;
		
	}
}
