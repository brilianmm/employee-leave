package com.employeeleave.brilianemployeleave.controller;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.employeeleave.brilianemployeleave.dto.PositionLeaveDTO;
import com.employeeleave.brilianemployeleave.models.PositionLeave;
import com.employeeleave.brilianemployeleave.repository.PositionLeaveRepository;

@RestController
@RequestMapping("/api/positionsleave")
public class PositionLeaveController {
	@Autowired
	PositionLeaveRepository repository;
	ModelMapper modelMapper = new ModelMapper();
	
	//create API
	@PostMapping("/create")
	public HashMap<String, Object> create(@Valid @RequestBody PositionLeave body){
		//create map for response
		HashMap<String, Object> result = new HashMap<String, Object>();

		// Create a Object ENtity and map from dto with modal mapper
		PositionLeave entity = modelMapper.map(body, PositionLeave.class);
		
		entity.setCreatedBy("admin");
		entity.setCreatedDate(java.sql.Date.valueOf(LocalDate.now()));
		entity.setUpdatedBy("admin");
		entity.setUpdatedDate(java.sql.Date.valueOf(LocalDate.now()));

		// saving data to repository
		repository.save(entity);

		// take the ID that was insert before
		body.setPositionLeaveId(entity.getPositionLeaveId());

		result.put("Status", "OK");
		result.put("Message", "successfully created");
		result.put("Data", body);

		return result;
	}
	
	
	@GetMapping("/read")
	public HashMap<String, Object> getAll(){
		HashMap<String, Object> resp = new HashMap<String, Object>();
		List<PositionLeave> list = repository.findAll();
		List<PositionLeaveDTO> listDTO = new ArrayList<PositionLeaveDTO>();
		
		for(PositionLeave entity : list) {
			PositionLeaveDTO dto = modelMapper.map(entity, PositionLeaveDTO.class);			
			listDTO.add(dto);
		}
		resp.put("status", 200);
		resp.put("message", "Success! Data All Reviewers.");
		resp.put("data", listDTO);
		return resp;
	}
	
	@GetMapping("/read/{id}")
	public HashMap<String, Object> getById(@PathVariable(value = "id") Long id) {
		HashMap<String, Object> resp = new HashMap<String, Object>();
		PositionLeave entity = repository.findById(id).get();

		PositionLeaveDTO dto = modelMapper.map(entity, PositionLeaveDTO.class);	
		
		resp.put("status", 200);
		resp.put("message", "Success!");
		resp.put("data", dto);
		return resp;
	}
	
	@PutMapping("/update/{id}")
	public HashMap<String, Object> update(@PathVariable(value = "id") Long id, @Valid @RequestBody PositionLeave body) {
		HashMap<String, Object> result = new HashMap<String, Object>();
		PositionLeave entity = repository.findById(id).get();
		
		body.setPositionLeaveId(entity.getPositionLeaveId());
		
		// Create a Object ENtity and map from dto with modal mapper
		entity = modelMapper.map(body, PositionLeave.class);
		entity.setCreatedBy(entity.getCreatedBy());
		entity.setCreatedDate(entity.getCreatedDate());
		entity.setUpdatedDate(java.sql.Date.valueOf(LocalDate.now()));
		entity.setUpdatedBy("admin");

		// saving data to repository
		repository.save(entity);

		// take the ID that was insert before
		body.setPositionLeaveId(entity.getPositionLeaveId());

		result.put("Status", "OK");
		result.put("Message", "successfully created");
		result.put("Data", body);

		return result;
	}
	
	@DeleteMapping("/delete/{id}")
	public HashMap<String, Object> delete(@PathVariable(value = "id") Long id){
		HashMap<String, Object> resp = new HashMap<String, Object>();
		PositionLeave entity = repository.findById(id).get();
		
		resp.put("status", 200);
		resp.put("message", "Success! Delete data from database.");
		
		repository.delete(entity);
		
		return resp;
	}
}
