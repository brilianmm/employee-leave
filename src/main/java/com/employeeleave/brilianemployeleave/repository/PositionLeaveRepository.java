package com.employeeleave.brilianemployeleave.repository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.employeeleave.brilianemployeleave.models.PositionLeave;

@Repository 
public interface PositionLeaveRepository extends JpaRepository<PositionLeave,Long>{

}
