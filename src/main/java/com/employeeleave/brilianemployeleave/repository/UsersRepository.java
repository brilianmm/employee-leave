package com.employeeleave.brilianemployeleave.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.employeeleave.brilianemployeleave.models.Users;

@Repository
public interface UsersRepository extends JpaRepository<Users, Long> {

}
