package com.employeeleave.brilianemployeleave.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.employeeleave.brilianemployeleave.models.Positions;

@Repository
public interface PositionsRepository extends JpaRepository<Positions, Long> {

}
